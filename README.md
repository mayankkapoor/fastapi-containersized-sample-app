# Fastapi Containersized Sample App

Build your Image
Go to your project directory.
Create a Dockerfile with:
```
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

COPY ./app /app
```
Create an app directory and enter in it.
Create a main.py file with:
```
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}
```
You should now have a directory structure like:
```
.
├── app
│   └── main.py
└── Dockerfile
```
Go to the project directory (in where your Dockerfile is, containing your app directory).
Build your FastAPI image:
```
docker build -t myimage .
```
Run a container based on your image:
```
docker run -d --name mycontainer -p 80:80 myimage
```
Now you have an optimized FastAPI server in a Docker container. Auto-tuned for your current server (and number of CPU cores).

Check it
You should be able to check it in your Docker container's URL, for example: http://192.168.99.100/items/5?q=somequery or http://127.0.0.1/items/5?q=somequery (or equivalent, using your Docker host).

You will see something like:

`{"item_id": 5, "q": "somequery"}`

Interactive API docs
Now you can go to http://192.168.99.100/docs or http://127.0.0.1/docs (or equivalent, using your Docker host).

You will see the automatic interactive API documentation (provided by Swagger UI):